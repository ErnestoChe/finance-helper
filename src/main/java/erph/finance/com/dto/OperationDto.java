package erph.finance.com.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OperationDto {

    private String category;

    private String operationType;

    private Integer amount;

    private String comment;

    private Long num;

}
