package erph.finance.com.dto;


import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WalletDto {

    private String name;

    private String comment;

    private Integer balance;

    private Integer debt;

}
