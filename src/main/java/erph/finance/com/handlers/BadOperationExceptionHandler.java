package erph.finance.com.handlers;

import erph.finance.com.exceptions.ApiError;
import erph.finance.com.exceptions.BadOperationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class BadOperationExceptionHandler {

    @ExceptionHandler(BadOperationException.class)
    public ResponseEntity<ApiError> badOperation(BadOperationException e) {
        return new ResponseEntity<>(new ApiError(e.getMessage()), HttpStatus.CONFLICT);
    }

}
