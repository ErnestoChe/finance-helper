package erph.finance.com.handlers;

import erph.finance.com.exceptions.ApiError;
import erph.finance.com.exceptions.NoSuchWalletException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class NoSuchWalletExceptionHandler {

    @ExceptionHandler(NoSuchWalletException.class)
    public ResponseEntity<ApiError> noSuchWallet(NoSuchWalletException e){
        return new ResponseEntity<>(new ApiError(e.getMessage()), HttpStatus.NOT_FOUND);
    }

}
