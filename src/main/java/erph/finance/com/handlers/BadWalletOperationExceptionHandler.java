package erph.finance.com.handlers;

import erph.finance.com.exceptions.ApiError;
import erph.finance.com.exceptions.BadWalletException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class BadWalletOperationExceptionHandler {

    @ExceptionHandler(BadWalletException.class)
    public ResponseEntity<ApiError> badWallet(BadWalletException e){
        return new ResponseEntity<>(new ApiError(e.getMessage()), HttpStatus.CONFLICT);
    }

}
