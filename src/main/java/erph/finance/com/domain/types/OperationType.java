package erph.finance.com.domain.types;

public enum OperationType {

    EXPENSE, INCOME

}
