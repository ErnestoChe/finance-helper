package erph.finance.com.domain.types;

public enum Category {
    //expences
    HOUSE, FOOD, ENTERTAINMENT, PERSONAL, TRANSPORT,
    //both
    DEBT,
    //incomes
    JOB, GIFT

}
