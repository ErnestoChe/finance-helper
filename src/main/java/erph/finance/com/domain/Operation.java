package erph.finance.com.domain;

import erph.finance.com.domain.types.Category;
import erph.finance.com.domain.types.OperationType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "\"operation\"")
public class Operation extends BaseEntity {

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "wallet_id")
    private Wallet wallet;

    private Long operationNumInWallet;

    @Enumerated(EnumType.STRING)
    private Category category;

    private String comment;

    private Integer amount;

    private OperationType operationType;
}
