package erph.finance.com.domain.form;


import lombok.*;


@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WalletForm {

    String name;

    String comment;

}
