package erph.finance.com.domain.form;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FilteredOperationForm {

    List<String> categories;

}
