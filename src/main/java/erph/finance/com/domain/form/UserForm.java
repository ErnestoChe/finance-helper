package erph.finance.com.domain.form;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserForm {

    @JsonProperty("username")
    String username;
    @JsonProperty("password")
    String password;
}
