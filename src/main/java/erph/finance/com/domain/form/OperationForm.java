package erph.finance.com.domain.form;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OperationForm {

    Integer amount;

    String category;

    String comment;

    String type;
}
