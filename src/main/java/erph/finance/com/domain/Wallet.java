package erph.finance.com.domain;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "\"wallet\"")
public class Wallet extends BaseEntity {

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "user_id")
    private User user;

    private Integer balance;

    private Integer debt;

    private String comment;

    private String name;

    @OneToMany(mappedBy = "wallet", cascade = CascadeType.ALL)
    private List<Operation> operations;

    //counter field for nums of operations, must be ++ when adding new
    private Long counter;

}
