package erph.finance.com.exceptions;

public class BadOperationException extends RuntimeException {

    public static final String cliche = "amount can't be negative %s";

    public BadOperationException(String message) {
        super(String.format(cliche, message));
    }

}
