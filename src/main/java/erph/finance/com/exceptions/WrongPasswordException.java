package erph.finance.com.exceptions;

public class WrongPasswordException extends RuntimeException {

    public static final String cliche = "wrong password";

    public WrongPasswordException() {
        super(cliche);
    }
}
