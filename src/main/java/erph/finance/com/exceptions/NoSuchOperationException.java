package erph.finance.com.exceptions;

public class NoSuchOperationException extends RuntimeException {

    public static final String cliche = "no operation with num %d";

    public NoSuchOperationException(Long num) {
        super(String.format(cliche, num));
    }

}
