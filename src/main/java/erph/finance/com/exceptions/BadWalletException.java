package erph.finance.com.exceptions;

public class BadWalletException extends RuntimeException {

    public static final String cliche = "name cant be blank";

    public BadWalletException() {
        super(String.format(cliche));
    }

}
