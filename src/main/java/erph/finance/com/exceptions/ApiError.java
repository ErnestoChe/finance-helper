package erph.finance.com.exceptions;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
public class ApiError {

    @JsonProperty("message")
    private final String message;

    @JsonProperty("time")
    private final LocalDateTime localDateTime;

    public ApiError(String message) {
        this(message, LocalDateTime.now());
    }

    public ApiError(String message, LocalDateTime localDateTime) {
        this.message = message;
        this.localDateTime = localDateTime;
    }
}
