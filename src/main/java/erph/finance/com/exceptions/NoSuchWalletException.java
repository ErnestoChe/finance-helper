package erph.finance.com.exceptions;

public class NoSuchWalletException extends RuntimeException {

    public static final String cliche = "no wallet with name %s";

    public NoSuchWalletException(String message) {
        super(String.format(cliche, message));
    }

}
