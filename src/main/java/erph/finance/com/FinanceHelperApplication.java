package erph.finance.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableJpaRepositories("erph.finance.com.repository")
public class FinanceHelperApplication {

    public static void main(String[] args) {
        SpringApplication.run(FinanceHelperApplication.class, args);
    }

}
