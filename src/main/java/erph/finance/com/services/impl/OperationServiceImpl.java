package erph.finance.com.services.impl;

import erph.finance.com.domain.Operation;
import erph.finance.com.domain.User;
import erph.finance.com.domain.Wallet;
import erph.finance.com.domain.form.OperationForm;
import erph.finance.com.domain.types.Category;
import erph.finance.com.domain.types.OperationType;
import erph.finance.com.dto.OperationDto;
import erph.finance.com.exceptions.BadOperationException;
import erph.finance.com.exceptions.NoSuchOperationException;
import erph.finance.com.exceptions.NoSuchWalletException;
import erph.finance.com.mapper.OperationMapper;
import erph.finance.com.repository.OperationRepository;
import erph.finance.com.repository.WalletRepository;
import erph.finance.com.services.CurrentUser;
import erph.finance.com.services.OperationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OperationServiceImpl implements OperationService {

    private final WalletRepository walletRepository;
    private final CurrentUser currentUser;
    private final OperationMapper operationMapper;
    private final OperationRepository operationRepository;


    @Override
    @Transactional
    public OperationDto addOperation(OperationForm operationForm, String walletName) {
        User user = currentUser.getCurrentUser();
        Wallet wallet = walletRepository.findByNameAndUser(walletName, user);
        if (wallet == null) {
            throw new NoSuchWalletException(walletName);
        }
        long l = wallet.getCounter();
        wallet.setCounter(l + 1);
        if (operationForm.getAmount() < 0) {
            throw new BadOperationException(String.valueOf(operationForm.getAmount()));
        }
        Operation operation = Operation.builder()
                .amount(operationForm.getAmount())
                .comment(operationForm.getComment())
                .category(Category.valueOf(operationForm.getCategory()))
                .operationType(OperationType.valueOf(operationForm.getType()))
                .wallet(wallet)
                .operationNumInWallet(l + 1)
                .build();

        wallet.getOperations().add(operation);

        //TODO вынести изменение баланса в отдельный метод?
        if (operation.getOperationType() == OperationType.INCOME) {
            wallet.setBalance(wallet.getBalance() + operation.getAmount());
        } else {
            wallet.setBalance(wallet.getBalance() - operation.getAmount());
        }

        return operationMapper.toDto(operationRepository.save(operation));
    }

    @Override
    public List<OperationDto> walletOperations(String walletName) {

        User user = currentUser.getCurrentUser();
        Wallet wallet = walletRepository.findByNameAndUser(walletName, user);
        if (wallet == null) {
            throw new NoSuchWalletException(walletName);
        }
        return operationMapper.listToDto(operationRepository.findOperationByWallet(wallet));
    }

    @Override
    @Transactional
    public OperationDto editOperation(OperationForm operationForm, String walletName, Long num) {
        User user = currentUser.getCurrentUser();
        Wallet wallet = walletRepository.findByNameAndUser(walletName, user);
        if (wallet == null) {
            throw new NoSuchWalletException(walletName);
        }
        if (operationForm.getAmount() < 0) {
            throw new BadOperationException(String.valueOf(operationForm.getAmount()));
        }
        Operation operation = operationRepository.findOperationByOperationNumInWalletAndWallet(num, wallet);
        if (operation == null) {
            throw new NoSuchOperationException(num);
        }
        //old values
        OperationType oldType = operation.getOperationType();
        int oldAmount = operation.getAmount();

        //reversing changing operation in wallet
        if (oldType == OperationType.INCOME) {
            wallet.setBalance(wallet.getBalance() - oldAmount);
        } else {
            wallet.setBalance(wallet.getBalance() + oldAmount);
        }
        //rewrite operation type if new exists
        OperationType newType;
        if (operationForm.getType() != null) {
            newType = OperationType.valueOf(operationForm.getType());
            operation.setOperationType(newType);
        } else {
            newType = oldType;
        }
        //rewrite amount if new exists
        int newAmount;
        if (operationForm.getAmount() != null) {
            newAmount = operationForm.getAmount();
            operation.setAmount(newAmount);
        } else {
            newAmount = oldAmount;
        }
        //new values is right because we rewriting only if new exists
        if (newType == OperationType.EXPENSE) {
            wallet.setBalance(wallet.getBalance() - newAmount);
        } else {
            wallet.setBalance(wallet.getBalance() + newAmount);
        }

        if (operationForm.getComment() != null)
            operation.setComment(operationForm.getComment());
        if (operationForm.getCategory() != null)
            operation.setCategory(Category.valueOf(operationForm.getCategory()));

        return operationMapper.toDto(operation);
    }

    @Override
    public void deleteOperation(String walletName, Long num) {
        User user = currentUser.getCurrentUser();
        Wallet wallet = walletRepository.findByNameAndUser(walletName, user);
        if (wallet == null) {
            throw new NoSuchWalletException(walletName);
        }
        Operation operation = operationRepository.findOperationByOperationNumInWalletAndWallet(num, wallet);
        if (operation == null) {
            throw new NoSuchOperationException(num);
        }
        if (operation.getOperationType() == OperationType.INCOME) {
            wallet.setBalance(wallet.getBalance() - operation.getAmount());
        } else {
            wallet.setBalance(wallet.getBalance() + operation.getAmount());
        }
        operationRepository.delete(operation);
    }
}
