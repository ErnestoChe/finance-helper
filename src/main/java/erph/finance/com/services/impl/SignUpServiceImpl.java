package erph.finance.com.services.impl;

import erph.finance.com.domain.User;
import erph.finance.com.domain.form.UserForm;
import erph.finance.com.dto.UserDto;
import erph.finance.com.exceptions.UserAlreadyExistException;
import erph.finance.com.mapper.UserMapper;
import erph.finance.com.repository.UserRepository;
import erph.finance.com.services.SignUpService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    @Transactional
    public UserDto signUp(UserForm userForm) {
        if (userRepository.findByUsername(userForm.getUsername()) == null) {
            User userToSave = User.builder()
                    .username(userForm.getUsername())
                    .password(bCryptPasswordEncoder.encode(userForm.getPassword()))
                    .build();
            User saved = userRepository.save(userToSave);

//            System.out.println(saved.getUsername() + " signed up with username");
//            System.out.println(saved.getPassword());

            return userMapper.toDto(saved);
        } else {
            throw new UserAlreadyExistException(userForm.getUsername());
        }
    }
}
