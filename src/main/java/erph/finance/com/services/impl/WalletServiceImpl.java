package erph.finance.com.services.impl;

import erph.finance.com.domain.User;
import erph.finance.com.domain.Wallet;
import erph.finance.com.domain.form.WalletForm;
import erph.finance.com.dto.WalletDto;
import erph.finance.com.exceptions.BadWalletException;
import erph.finance.com.exceptions.NoSuchWalletException;
import erph.finance.com.mapper.WalletMapper;
import erph.finance.com.repository.WalletRepository;
import erph.finance.com.services.CurrentUser;
import erph.finance.com.services.WalletService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
public class WalletServiceImpl implements WalletService {

    private final WalletRepository walletRepository;
    private final CurrentUser currentUser;
    private final WalletMapper walletMapper;

    @Override
    @Transactional
    public WalletDto addWallet(WalletForm walletForm) {

        User user = currentUser.getCurrentUser();

        if (walletForm.getName() == null) {
            throw new BadWalletException();
        }
        Wallet toSave = Wallet.builder()
                .name(walletForm.getName())
                .comment(walletForm.getComment())
                .user(user)
                .balance(0)
                .debt(0)
                .counter(0L)
                .build();
        user.getWallets().add(toSave);

        return walletMapper.toDto(walletRepository.save(toSave));
    }

    @Override
    public List<WalletDto> allWallets() {

        List<Wallet> byUser = walletRepository.findByUserId(currentUser.getCurrentUserId());

        return walletMapper.listToDto(byUser);
    }

    @Override
    public WalletDto walletByName(String walletName) {

        Wallet wallet = walletRepository.findByNameAndUserId(walletName, currentUser.getCurrentUserId());
        if (wallet == null) {
            throw new NoSuchWalletException(walletName);
        }
        return walletMapper.toDto(wallet);
    }

    @Override
    public void deleteWallet(String walletName) {

        Wallet wallet = walletRepository.findByNameAndUserId(walletName, currentUser.getCurrentUserId());
        if (wallet == null) {
            throw new NoSuchWalletException(walletName);
        }
        walletRepository.delete(wallet);
    }

    @Override
    @Transactional
    public WalletDto editWallet(WalletForm walletForm, String walletName) {

        Wallet wallet = walletRepository.findByNameAndUserId(walletName, currentUser.getCurrentUserId());
        if (wallet == null) {
            throw new NoSuchWalletException(walletName);
        }

        if (walletForm.getName() != null)
            wallet.setName(walletForm.getName());
        if (walletForm.getComment() != null)
            wallet.setComment(walletForm.getComment());

        return walletMapper.toDto(wallet);
    }
}
