package erph.finance.com.services.impl;

import erph.finance.com.domain.Operation;
import erph.finance.com.domain.User;
import erph.finance.com.domain.Wallet;
import erph.finance.com.domain.form.FilteredOperationForm;
import erph.finance.com.domain.types.Category;
import erph.finance.com.domain.types.OperationType;
import erph.finance.com.dto.OperationDto;
import erph.finance.com.exceptions.NoSuchWalletException;
import erph.finance.com.mapper.OperationMapper;
import erph.finance.com.repository.OperationRepository;
import erph.finance.com.repository.WalletRepository;
import erph.finance.com.services.CurrentUser;
import erph.finance.com.services.OperationFilterService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OperationFilterServiceImpl implements OperationFilterService {

    private final WalletRepository walletRepository;
    private final CurrentUser currentUser;
    private final OperationMapper operationMapper;
    private final OperationRepository operationRepository;


    @Override
    public List<OperationDto> getExpenses(String walletName, OperationType operationType) {

        User user = currentUser.getCurrentUser();
        Wallet wallet = walletRepository.findByNameAndUser(walletName, user);
        if (wallet == null) {
            throw new NoSuchWalletException(walletName);
        }

        List<Operation> operationByWalletAndOperationType
                = operationRepository.findOperationByWalletAndOperationType(wallet, operationType);

        return operationMapper.listToDto(operationByWalletAndOperationType);
    }

    @Override
    public List<OperationDto> getByCategories(String walletName, FilteredOperationForm filteredOperationForm) {

        User user = currentUser.getCurrentUser();
        Wallet wallet = walletRepository.findByNameAndUser(walletName, user);
        if (wallet == null) {
            throw new NoSuchWalletException(walletName);
        }

        List<Category> categories = new ArrayList<>();
        for (String str : filteredOperationForm.getCategories()) {
            categories.add(Category.valueOf(str));
        }
        return operationMapper.
                listToDto(operationRepository.findOperationByCategoryInAndWallet(categories, wallet));
    }
}
