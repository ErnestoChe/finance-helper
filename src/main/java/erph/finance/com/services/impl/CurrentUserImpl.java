package erph.finance.com.services.impl;

import erph.finance.com.domain.User;
import erph.finance.com.repository.UserRepository;
import erph.finance.com.services.CurrentUser;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CurrentUserImpl implements CurrentUser {

    private final UserRepository userRepository;

    public User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String name = authentication.getName();
        return userRepository.findByUsername(name);
    }

    @Override
    public Long getCurrentUserId() {
        return getCurrentUser().getId();
    }
}
