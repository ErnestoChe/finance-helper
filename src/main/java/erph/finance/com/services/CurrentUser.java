package erph.finance.com.services;

import erph.finance.com.domain.User;

public interface CurrentUser {
    //TODO name (user getter)

    User getCurrentUser();

    Long getCurrentUserId();
}
