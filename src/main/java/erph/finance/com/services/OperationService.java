package erph.finance.com.services;

import erph.finance.com.domain.form.OperationForm;
import erph.finance.com.dto.OperationDto;

import java.util.List;

public interface OperationService {

    OperationDto addOperation(OperationForm operationForm, String walletName);

    List<OperationDto> walletOperations(String walletName);

    OperationDto editOperation(OperationForm operationForm, String walletName, Long num);

    void deleteOperation(String walletName, Long num);

}
