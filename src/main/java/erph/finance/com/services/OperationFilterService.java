package erph.finance.com.services;

import erph.finance.com.domain.form.FilteredOperationForm;
import erph.finance.com.domain.types.OperationType;
import erph.finance.com.dto.OperationDto;

import java.util.List;

public interface OperationFilterService {

    List<OperationDto> getExpenses(String walletName, OperationType operationType);

    List<OperationDto> getByCategories(String walletName, FilteredOperationForm filteredOperationForm);
}
