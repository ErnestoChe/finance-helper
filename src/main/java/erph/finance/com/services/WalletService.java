package erph.finance.com.services;

import erph.finance.com.domain.form.WalletForm;
import erph.finance.com.dto.WalletDto;

import java.util.List;

public interface WalletService {

    WalletDto addWallet(WalletForm walletForm);
    //wallets of a current users
    List<WalletDto> allWallets();

    WalletDto walletByName(String name);

    void deleteWallet(String name);

    WalletDto editWallet(WalletForm walletForm, String walletName);

}
