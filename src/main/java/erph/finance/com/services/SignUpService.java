package erph.finance.com.services;


import erph.finance.com.domain.form.UserForm;
import erph.finance.com.dto.UserDto;

public interface SignUpService {

    UserDto signUp(UserForm userForm);

}
