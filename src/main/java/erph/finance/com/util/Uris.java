package erph.finance.com.util;

public interface Uris {
    String SIGNUP = "/signup";

    String USER = "/users";

    String OPERATION = "/operation";

    String WALLET = "/wallet";

    String ADD = "/add";

    String EDIT = "/edit";

    String ALL = "/all";

    String DELETE = "/delete";


}
