package erph.finance.com.repository;

import erph.finance.com.domain.Operation;
import erph.finance.com.domain.Wallet;
import erph.finance.com.domain.types.Category;
import erph.finance.com.domain.types.OperationType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OperationRepository extends JpaRepository<Operation, Long> {

    List<Operation> findOperationByWallet(Wallet wallet);

    List<Operation> findOperationByWalletAndOperationType(Wallet wallet, OperationType operationType);

    List<Operation> findOperationByCategoryInAndWallet(List<Category> categories, Wallet wallet);

    List<Operation> findAllByAmountGreaterThan(int amount);

    List<Operation> findAllByAmountLessThan(int amount);

    List<Operation> findAllByAmountBetween(int from, int to);

    @Query("SELECT COUNT(op) FROM Operation op WHERE op.wallet=:wallet")
    long operationsInWallet(@Param("wallet") Wallet wallet);

    Operation findOperationByOperationNumInWalletAndWallet(Long num, Wallet wallet);

}
