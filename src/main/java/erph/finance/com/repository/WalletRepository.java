package erph.finance.com.repository;

import erph.finance.com.domain.User;
import erph.finance.com.domain.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WalletRepository extends JpaRepository<Wallet, Long> {

    List<Wallet> findByUser(User user);

    List<Wallet> findByUserId(Long id);

    Wallet findByNameAndUser(String name, User user);

    Wallet findByNameAndUserId(String name, Long id);

}
