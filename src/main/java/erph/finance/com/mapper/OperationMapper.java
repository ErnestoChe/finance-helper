package erph.finance.com.mapper;

import erph.finance.com.domain.Operation;
import erph.finance.com.dto.OperationDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper
public interface OperationMapper {

    @Mapping(source = "operationNumInWallet", target = "num")
    OperationDto toDto(Operation operation);

    @Mapping(source = "operationNumInWallet", target = "num")
    List<OperationDto> listToDto(List<Operation> operations);

}
