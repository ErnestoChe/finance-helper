package erph.finance.com.mapper;

import erph.finance.com.domain.User;
import erph.finance.com.dto.UserDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {

    UserDto toDto(User user);

    List<UserDto> listToDto(List<User> users);
}
