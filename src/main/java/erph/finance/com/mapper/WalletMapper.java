package erph.finance.com.mapper;

import erph.finance.com.domain.Wallet;
import erph.finance.com.dto.WalletDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface WalletMapper {

    WalletDto toDto(Wallet wallet);

    List<WalletDto> listToDto(List<Wallet> wallets);

}
