package erph.finance.com.controllers;

import erph.finance.com.domain.form.FilteredOperationForm;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping("/test")
    public void signUp(@RequestBody FilteredOperationForm form) {

        for (String s: form.getCategories()) {
            System.out.println(s);
        }

    }

}
