package erph.finance.com.controllers.wallet;


import erph.finance.com.domain.form.WalletForm;
import erph.finance.com.dto.WalletDto;
import erph.finance.com.services.WalletService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static erph.finance.com.util.Uris.*;

@RestController
@RequestMapping(WALLET)
@RequiredArgsConstructor
public class WalletAddController {

    private final WalletService walletService;

    @PostMapping(ADD)
    public ResponseEntity<WalletDto> addWallet(@RequestBody WalletForm walletForm) {
        return new ResponseEntity<>(walletService.addWallet(walletForm), HttpStatus.CREATED);
    }


    @PutMapping(EDIT + "/{name}")
    public ResponseEntity<WalletDto> editWallet(@PathVariable String name, @RequestBody WalletForm walletForm) {
        return new ResponseEntity<>(walletService.editWallet(walletForm, name), HttpStatus.ACCEPTED);
    }

}
