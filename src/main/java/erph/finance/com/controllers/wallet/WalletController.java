package erph.finance.com.controllers.wallet;


import erph.finance.com.dto.WalletDto;
import erph.finance.com.services.WalletService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static erph.finance.com.util.Uris.ALL;
import static erph.finance.com.util.Uris.WALLET;

@RestController
@RequestMapping(WALLET)
@RequiredArgsConstructor
public class WalletController {

    private final WalletService walletService;

    @GetMapping(ALL)
    public List<WalletDto> usersWallets() {
        return walletService.allWallets();
    }

    @GetMapping("/{name}")
    public WalletDto walletByName(@PathVariable String name) {
        return walletService.walletByName(name);
    }

    @DeleteMapping("/{name}")
    public ResponseEntity<?> deleteWallet(@PathVariable String name) {
        walletService.deleteWallet(name);
        return ResponseEntity.ok(HttpStatus.OK);
    }

}
