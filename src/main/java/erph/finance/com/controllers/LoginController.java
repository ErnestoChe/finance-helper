package erph.finance.com.controllers;

import erph.finance.com.domain.form.UserForm;
import erph.finance.com.dto.UserDto;
import erph.finance.com.services.SignUpService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static erph.finance.com.util.Uris.SIGNUP;
import static erph.finance.com.util.Uris.USER;

@RestController
@RequestMapping(USER)
@RequiredArgsConstructor
public class LoginController {

    private final SignUpService signUpService;

    @PostMapping(SIGNUP)
    public ResponseEntity<UserDto> signUp(@RequestBody UserForm userForm) {

        UserDto userDto = signUpService.signUp(userForm);
        return new ResponseEntity<>(userDto, HttpStatus.CREATED);
    }

}
