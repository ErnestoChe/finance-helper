package erph.finance.com.controllers.operation;


import erph.finance.com.domain.form.FilteredOperationForm;
import erph.finance.com.domain.types.OperationType;
import erph.finance.com.dto.OperationDto;
import erph.finance.com.services.OperationFilterService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static erph.finance.com.util.Uris.*;

@RestController
@RequestMapping(OPERATION)
@RequiredArgsConstructor
public class FilteredOperationController{

    private final OperationFilterService operationFilterService;

    @GetMapping("/expense/{walletName}")
    public List<OperationDto> getExpenses(@PathVariable String walletName) {

        return operationFilterService.getExpenses(walletName, OperationType.EXPENSE);

    }

    @GetMapping("/income/{walletName}")
    public List<OperationDto> getIncomes(@PathVariable String walletName) {

        return operationFilterService.getExpenses(walletName, OperationType.INCOME);

    }

    @GetMapping("/categories/{walletName}")
    public List<OperationDto> getByCategories(
            @PathVariable String walletName,
            @RequestBody FilteredOperationForm filteredOperationForm) {

        return operationFilterService.getByCategories(walletName, filteredOperationForm);

    }

}
