package erph.finance.com.controllers.operation;

import erph.finance.com.domain.form.OperationForm;
import erph.finance.com.dto.OperationDto;
import erph.finance.com.services.OperationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import static erph.finance.com.util.Uris.*;

@RestController
@RequestMapping(OPERATION)
@RequiredArgsConstructor
public class OperationAddController {

    private final OperationService operationService;

    @PostMapping(ADD + "/{walletName}")
    public OperationDto addOperation(@PathVariable String walletName, @RequestBody OperationForm operationForm) {

        return operationService.addOperation(operationForm, walletName);
    }

    //TODO edit
    @PutMapping(EDIT + "/{walletName}/{num}")
    public OperationDto editOperation(
            @PathVariable String walletName,
            @PathVariable Long num,
            @RequestBody OperationForm operationForm) {

        return operationService.editOperation(operationForm, walletName, num);

    }

}
