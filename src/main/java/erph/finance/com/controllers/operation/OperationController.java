package erph.finance.com.controllers.operation;


import erph.finance.com.dto.OperationDto;
import erph.finance.com.services.OperationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static erph.finance.com.util.Uris.*;

@RestController
@RequestMapping(OPERATION)
@RequiredArgsConstructor
public class OperationController {

    private final OperationService operationService;

    @GetMapping(ALL + "/{walletName}")
    public List<OperationDto> allOperation(@PathVariable String walletName) {

        return operationService.walletOperations(walletName);

    }

    @DeleteMapping(DELETE + "/{walletName}/{num}")
    public ResponseEntity<?> deleteOperation(@PathVariable String walletName, @PathVariable Long num) {

        operationService.deleteOperation(walletName, num);

        return ResponseEntity.ok(HttpStatus.OK);
    }

}
