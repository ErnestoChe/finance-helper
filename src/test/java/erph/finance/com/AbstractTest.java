package erph.finance.com;

import erph.finance.com.repository.OperationRepository;
import erph.finance.com.repository.UserRepository;
import erph.finance.com.repository.WalletRepository;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.AfterEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@RequiredArgsConstructor
@AutoConfigureMockMvc
public class AbstractTest {

    @LocalServerPort
    protected int port;

    protected TestRestTemplate testRestTemplate;

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected UserRepository userRepository;
    @Autowired
    protected WalletRepository walletRepository;
    @Autowired
    protected OperationRepository operationRepository;

    @AfterEach
    void flush(){
        operationRepository.deleteAll();
        operationRepository.flush();
        walletRepository.deleteAll();
        walletRepository.flush();
        userRepository.deleteAll();
        userRepository.flush();
    }

}
