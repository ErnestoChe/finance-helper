package erph.finance.com.services.impl;

import erph.finance.com.AbstractTest;
import erph.finance.com.domain.User;
import erph.finance.com.domain.Wallet;
import erph.finance.com.domain.form.OperationForm;
import erph.finance.com.dto.OperationDto;
import erph.finance.com.services.OperationService;
import erph.finance.com.services.WalletService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(MockitoJUnitRunner.class)
class OperationServiceImplTest extends AbstractTest {

    @Autowired
    OperationService operationService;
    @Autowired
    WalletService walletService;

    @BeforeEach
    void init() {
        User user = User.builder().username("admin").build();
        Wallet toSave = Wallet.builder()
                .name("wallet1")
                .comment("comm1")
                .user(user)
                .balance(0)
                .debt(0)
                .counter(0L)
                .build();
        walletRepository.save(toSave);
    }

    @Test
    @WithMockUser(username = "admin")
    void getId() {
        OperationForm operationForm = OperationForm.builder()
                .amount(1)
                .category("JOB")
                .type("INCOME")
                .comment("comm")
                .build();

        OperationDto operationDto = operationService.addOperation(operationForm, "wallet1");
        assertAll(
                ()->assertEquals(operationDto.getAmount(),1)
        );
    }

}