package erph.finance.com.services.impl;

import erph.finance.com.AbstractTest;
import erph.finance.com.domain.form.UserForm;
import erph.finance.com.dto.UserDto;
import erph.finance.com.services.SignUpService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class SignUpServiceImplTest extends AbstractTest {

    @Autowired
    SignUpService signUpService;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @BeforeEach
    void init(){

    }

    @Test
    void signUp() {

        UserDto userDto = signUpService.signUp(UserForm.builder().username("user").password("pass").build());
        assertAll(
                ()->assertEquals(userDto.getUsername(), "user")
        );
    }
}