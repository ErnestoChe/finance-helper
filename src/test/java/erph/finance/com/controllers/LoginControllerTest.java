package erph.finance.com.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import erph.finance.com.AbstractTest;
import erph.finance.com.domain.User;
import erph.finance.com.domain.form.UserForm;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.nio.charset.Charset;

import static erph.finance.com.util.Uris.SIGNUP;
import static erph.finance.com.util.Uris.USER;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


class LoginControllerTest extends AbstractTest {

    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @Test
    public void greetingShouldReturnDefaultMessage() throws Exception {

        UserForm userForm = UserForm.builder().username("user1").password("pass").build();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(userForm);

        this.mockMvc.perform(
                post(USER + SIGNUP)
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(requestJson))
                .andExpect(status().isCreated())
                .andExpect(content().json("{'username':'user1'}"));
    }

    @Test
    public void signUpWithExistingUserResultsWithException() throws Exception {

        userRepository.save(User.builder().username("user1").password("123").build());

        UserForm userForm = UserForm.builder().username("user1").password("pass").build();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(userForm);

        this.mockMvc.perform(
                post(USER + SIGNUP)
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(requestJson))
                .andExpect(status().isConflict());

    }

}