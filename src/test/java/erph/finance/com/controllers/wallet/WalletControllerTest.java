package erph.finance.com.controllers.wallet;

import erph.finance.com.AbstractTest;
import erph.finance.com.dto.WalletDto;
import erph.finance.com.exceptions.NoSuchWalletException;
import erph.finance.com.repository.WalletRepository;
import erph.finance.com.services.CurrentUser;
import erph.finance.com.services.WalletService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;
import java.util.List;

import static erph.finance.com.util.Uris.ALL;
import static erph.finance.com.util.Uris.WALLET;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class WalletControllerTest extends AbstractTest {

    @MockBean
    WalletService walletService;

    @MockBean
    WalletRepository walletRepository;

    @MockBean
    CurrentUser currentUser;

    @Test
    @WithMockUser(username = "user1")
    void getAllWalletsWithAuthUserExpect200() throws Exception {

        List<WalletDto> walletDtos = new ArrayList<>();
        walletDtos.add(WalletDto.builder().name("wallet1").comment("comment1").build());
        walletDtos.add(WalletDto.builder().name("wallet2").comment("comment2").build());
        walletDtos.add(WalletDto.builder().name("wallet3").comment("comment3").build());

        when(walletService.allWallets()).thenReturn(walletDtos);

        mockMvc.perform(
                get(WALLET+ALL)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].name").value("wallet1"))
                .andExpect(status().isOk());
    }

    @Test
    void getAllWalletsWithOutAuthUserExpect403() throws Exception {

        List<WalletDto> walletDtos = new ArrayList<>();
        walletDtos.add(WalletDto.builder().name("wallet1").comment("comment1").build());
        walletDtos.add(WalletDto.builder().name("wallet2").comment("comment2").build());

        when(walletService.allWallets()).thenReturn(walletDtos);

        mockMvc.perform(
                get(WALLET+ALL)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "user1")
    void getWalletByNameWithAuthUserExpects200() throws Exception {


        WalletDto build = WalletDto.builder().name("wallet1").comment("comment1").build();

        when(walletService.walletByName("wallet1")).thenReturn(build);

        mockMvc.perform(
                get(WALLET+"/wallet1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("wallet1"))
                .andExpect(jsonPath("$.comment").value("comment1"))
                .andExpect(status().isOk());
    }

    @Test
    void getWalletByNameWithOutAuthUserExpects403() throws Exception {


        WalletDto build = WalletDto.builder().name("wallet1").comment("comment1").build();

        when(walletService.walletByName("wallet1")).thenReturn(build);

        mockMvc.perform(
                get(WALLET+"/wallet1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "user1")
    void getNonExistentWalletByNameWithOutAuthUserExpects404() throws Exception {

        when(walletService.walletByName("wallet1")).thenThrow(new NoSuchWalletException("bad"));

        mockMvc.perform(
                get(WALLET+"/wallet1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "user1")
    void deleteWalletWithAuthUserExpects200() throws Exception {

        doNothing().when(walletService).deleteWallet("wallet1");

        mockMvc.perform(
                delete(WALLET+"/wallet1"))
                .andExpect(status().isOk());
    }

    @Test
    void deleteWalletWithOutAuthUserExpects403() throws Exception {

        doNothing().when(walletService).deleteWallet("wallet1");

        mockMvc.perform(
                delete(WALLET+"/wallet1"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "user1")
    void deleteNonExistWalletWithAuthUserExpects404() throws Exception {

        doThrow(new NoSuchWalletException("wallet1")).when(walletService).deleteWallet("wallet1");

        mockMvc.perform(
                delete(WALLET+"/wallet1"))
                .andExpect(status().isNotFound());
    }
}