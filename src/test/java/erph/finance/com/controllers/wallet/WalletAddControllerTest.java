package erph.finance.com.controllers.wallet;


import erph.finance.com.AbstractTest;
import erph.finance.com.domain.User;
import erph.finance.com.domain.Wallet;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.context.WebApplicationContext;

import static erph.finance.com.util.Uris.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
class WalletAddControllerTest extends AbstractTest {

    @Autowired
    private WebApplicationContext context;

    @Test
    @WithMockUser(username = "user1")
    void getWalletWithUserExpect200() throws Exception {

        mockMvc.perform(put(WALLET+ALL))
                .andExpect(status().is4xxClientError());

        mockMvc.perform(get(WALLET+ALL))
                .andExpect(status().isOk());

    }

    @Test
    @WithMockUser(username = "user1")
    void addWalletWithUserExpect201() throws Exception {
        userRepository.save(User.builder().username("user1").build());

        mockMvc.perform(
                post(WALLET + ADD)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\": \"wallet1\", \"comment\": \"comment1\"}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("wallet1"))
                .andExpect(jsonPath("$.comment").value("comment1"))
                .andExpect(jsonPath("$.balance").value("0"))
                .andExpect(status().isCreated());
    }

    @Test
    void notAuthAddWalletExpect403() throws Exception {

        mockMvc.perform(
                post(WALLET+ADD)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\": \"wallet1\", \"comment\": \"comment1\"}"))
                .andExpect(status().isForbidden());

    }

    @Test
    void notAuthGetWalletExpect403() throws Exception {

        mockMvc.perform(
                get(WALLET+ADD))
                .andExpect(status().isForbidden());

    }

    @Test
    @WithMockUser(username = "user1")
    void addBadWalletWithUserExpectNotAccepted() throws Exception {

        //bad = null name
        userRepository.save(User.builder().username("user1").build());

        mockMvc.perform(
                post(WALLET + ADD)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"walletname\": \"wallet1\"}")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
    }

    @Test
    @WithMockUser(username = "user1")
    void editWalletWithUserExpect202() throws Exception {

        User user1 = userRepository.save(User.builder().username("user1").build());
        String walletName = "name";
        walletRepository.save(Wallet.builder().name(walletName).comment("comment").user(user1).build());

        mockMvc.perform(
                put(WALLET+EDIT + "/" + walletName)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\": \"name1\"}")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("name1"))
                .andExpect(jsonPath("$.comment").value("comment"))
                .andExpect(status().isAccepted());
    }


    @Test
    void notAuthUserEditWalletExpect403() throws Exception {

        User user1 = userRepository.save(User.builder().username("user1").build());
        String walletName = "name";
        walletRepository.save(Wallet.builder().name(walletName).comment("comment").user(user1).build());

        mockMvc.perform(
                put(WALLET+EDIT + "/" + walletName)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\": \"name1\"}")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "user1")
    void editNonExistentWalletWithUserExpectNotAccepted() throws Exception {

        User user1 = userRepository.save(User.builder().username("user1").build());
        String walletName = "name";
        String nonExistentWalletName = "wallet123";
        walletRepository.save(Wallet.builder().name(walletName).comment("comment").user(user1).build());

        mockMvc.perform(
                put(WALLET+EDIT + "/" + nonExistentWalletName)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"aoao\": \"name1\"}")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value("no wallet with name " + nonExistentWalletName))
                .andExpect(status().isNotFound());
    }
}